from calendar import weekday
from sys import stdin

mm, dd, yyyy = map(int, next(stdin).split())
print(('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY',
       'FRIDAY', 'SATURDAY', 'SUNDAY')[weekday(yyyy, mm, dd)])
