from datetime import datetime
from sys import stdin


def to_date(date_string):
    return datetime.strptime(date_string.rstrip(), '%a %d %b %Y %H:%M:%S %z')

next(stdin)
for a, b in zip(stdin, stdin):
    print(int(abs(to_date(a) - to_date(b)).total_seconds()))
