from re import compile, findall
from sys import stdin

REGEX = compile(r'(#[0-9a-fA-F]{3}|#[0-9a-fA-F]{6})[;,)]')
[print(match) for match in findall(REGEX, stdin.read())]
