from re import compile, match
from sys import stdin

REGEX = compile(r'^[789]\d{9}$')

next(stdin)
[print('YES' if match(REGEX, number) else 'NO') for number in stdin]
