from sys import stdin

next(stdin)
for _, a, __, b in zip(stdin, stdin, stdin, stdin):
    print(set(a.split()).issubset(b.split()))
