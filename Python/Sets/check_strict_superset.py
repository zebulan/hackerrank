from sys import stdin

a = set(next(stdin).split())
next(stdin)
strict_superset = True
for line in stdin:
    current = set(line.split())
    if current == a or not a.issuperset(current):
        strict_superset = False
        break
print(strict_superset)
