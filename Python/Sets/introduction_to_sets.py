from statistics import mean
from sys import stdin

next(stdin)
print(mean(set(map(int, next(stdin).split()))))
