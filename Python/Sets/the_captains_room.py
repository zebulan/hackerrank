from collections import Counter
from sys import stdin

next(stdin)
print(min(Counter(next(stdin).split()).items(), key=lambda a: a[1])[0])
