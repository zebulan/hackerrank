from sys import stdin

next(stdin)
for line in stdin:
    try:
        a, b = map(int, line.split())
        print(a // b)
    except (ValueError, ZeroDivisionError) as e:
        print('Error Code: {}'.format(e))
