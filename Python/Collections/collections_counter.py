# from sys import stdin
#
# money_earned = 0
# next(stdin)
# shoes = list(map(int, next(stdin).split()))
# next(stdin)
# for line in stdin:
#     size, price = map(int, line.split())
#     try:
#         shoes.remove(size)
#         money_earned += price
#     except ValueError:
#         pass
# print(money_earned)

from collections import Counter
from sys import stdin

money_earned = 0
next(stdin)
shoes = Counter(next(stdin).split())
next(stdin)
for line in stdin:
    size, price = line.split()
    if shoes[size] > 0:
        shoes[size] -= 1
        money_earned += int(price)
print(money_earned)
