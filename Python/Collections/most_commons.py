from collections import Counter
from sys import stdin

[print(k, v) for k, v in sorted(Counter(next(stdin)).items(),
                                key=lambda a: (-a[1], a[0]))[:3]]
