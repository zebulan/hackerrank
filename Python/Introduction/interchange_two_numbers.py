from sys import stdin

[print(a) for a in reversed(tuple(map(int, stdin)))]
