from sys import stdin

[print(a ** 2) for a in range(int(next(stdin)))]

# # python 2
# for a in xrange(int(next(stdin))):
#     print a ** 2
