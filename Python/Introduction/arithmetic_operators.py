from operator import add, sub, mul
from sys import stdin

a, b = map(int, stdin)
print('{}\n{}\n{}'.format(add(a, b), sub(a, b), mul(a, b)))
