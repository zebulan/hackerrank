from statistics import mean
from sys import stdin

students = {}
next(stdin)
for line in stdin:
    try:
        name, *marks = line.split()
        students[name] = mean(map(float, marks))
    except ValueError:
        print('{:.2f}'.format(students[line.rstrip()]))
