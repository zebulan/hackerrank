from operator import floordiv, truediv
from sys import stdin

a, b = map(int, stdin)
print('{}\n{}'.format(floordiv(a, b), truediv(a, b)))
