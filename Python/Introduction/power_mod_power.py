from sys import stdin

a, b, c = map(int, stdin)
print('{}\n{}'.format(pow(a, b), pow(a, b, c)))
