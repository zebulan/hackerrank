from sys import stdin

_, *lines, k = (list(map(int, a.split())) for a in stdin)
for line in sorted(lines, key=lambda a: a[k[0]]):
    print(' '.join(map(str, line)))
