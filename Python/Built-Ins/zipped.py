from statistics import mean
from sys import stdin

next(stdin)
[print(mean(b)) for b in zip(*(map(float, a.split()) for a in stdin))]
