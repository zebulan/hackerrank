from itertools import combinations_with_replacement as combos_w_r
from sys import stdin

string, num = next(stdin).split()
[print(''.join(a)) for a in combos_w_r(sorted(string), int(num))]
