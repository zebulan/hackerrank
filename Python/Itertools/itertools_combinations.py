from itertools import combinations
from sys import stdin

string, num = next(stdin).split()
string = sorted(string)
for a in range(1, int(num) + 1):
    [print(''.join(a)) for a in combinations(string, a)]
