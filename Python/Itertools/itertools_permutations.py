from itertools import permutations
from sys import stdin

string, num = next(stdin).split()
[print(''.join(a)) for a in permutations(sorted(string), int(num))]
