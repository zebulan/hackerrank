from itertools import product
from sys import stdin

print(' '.join(str(a) for a in product(map(int, next(stdin).split()),
                                       map(int, next(stdin).split()))))
