from sys import stdin


def fibonacci():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

print([f ** 3 for _, f in zip(range(int(next(stdin))), fibonacci())])
# print(list(map(lambda a: a[1] ** 3, zip(range(int(next(stdin))), fibonacci()))))
