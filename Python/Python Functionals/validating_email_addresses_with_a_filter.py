from re import compile, match
from sys import stdin

REGEX = compile(r'^[a-z0-9_-]+@[a-z0-9]+\.[a-z]{1,3}$')
print(sorted(e.rstrip() for e in stdin if match(REGEX, e)))
