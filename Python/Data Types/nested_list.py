from collections import defaultdict
from sys import stdin

marks = defaultdict(list)
next(stdin)
for student, mark in zip(stdin, stdin):
    marks[float(mark)].append(student.rstrip())

[print(a) for a in sorted(marks[sorted(marks)[1]])]
