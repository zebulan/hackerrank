from sys import stdin

my_list = []
next(stdin)
for line in stdin:
    current = line.split()
    command = current[0]
    args = current[1:]
    if command == 'append':
        my_list.append(*map(int, args))
    elif command == 'count':
        print(my_list.count(*map(int, args)))
    elif command == 'index':
        print(my_list.index(*map(int, args)))
    elif command == 'insert':
        my_list.insert(*map(int, args))
    elif command == 'pop':
        my_list.pop()
    elif command == 'print':
        print(my_list)
    elif command == 'remove':
        try:
            my_list.remove(*map(int, args))
        except ValueError:
            pass
    elif command == 'reverse':
        my_list.reverse()
    elif command == 'sort':
        my_list.sort()
