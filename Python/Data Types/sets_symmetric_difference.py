from sys import stdin

m, n = (set(map(int, a.split())) for i, a in enumerate(stdin) if i % 2)
[print(b) for b in sorted(m.symmetric_difference(n))]
