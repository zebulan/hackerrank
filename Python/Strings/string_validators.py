from sys import stdin

string = next(stdin)
alnum = alpha = digit = lower = upper = False
for a in string:
    if a.isalpha():
        alpha = True
        alnum = True
        if a.islower():
            lower = True
        else:
            upper = True
    elif a.isdigit():
        digit = True
        alnum = True
print('{}\n{}\n{}\n{}\n{}\n'.format(alnum, alpha, digit, lower, upper))
