from sys import stdin

OUTPUT = '{0:>{1}} {0:>{1}o} {0:>{1}X} {0:>{1}b}'.format
NUM = int(next(stdin))
WIDTH = len(bin(NUM)) - 2
[print(OUTPUT(a, WIDTH)) for a in range(1, NUM + 1)]
