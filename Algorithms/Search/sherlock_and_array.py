from sys import stdin

next(stdin)

for _, nums in zip(stdin, stdin):
    numbers = list(map(int, nums.split()))
    l_dex = 0
    l_sum = numbers[0]
    r_dex = len(numbers) - 1
    r_sum = numbers[-1]

    while l_dex != r_dex:
        if l_sum < r_sum:
            l_dex += 1
            l_sum += numbers[l_dex]
        else:
            r_dex -= 1
            r_sum += numbers[r_dex]
    print(('NO', 'YES')[l_sum == r_sum])
