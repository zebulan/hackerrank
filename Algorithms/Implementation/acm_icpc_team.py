from collections import defaultdict
from sys import stdin

next(stdin)
rows = list(map(lambda a: int(a, 2), stdin))
counts = defaultdict(int)
for i, b in enumerate(rows):
    for c in rows[i + 1:]:
        counts[format(b | c, 'b').count('1')] += 1
print('{}\n{}'.format(*max(counts.items())))
