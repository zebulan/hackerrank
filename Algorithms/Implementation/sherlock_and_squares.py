from sys import stdin

squares = []
i = n = 1
k = 10 ** 9
while n <= k:
    squares.append(n)
    n += 2 * i + 1
    i += 1

next(stdin)
for line in stdin:
    start, stop = map(int, line.split())
    print(sum(start <= a <= stop for a in squares))
