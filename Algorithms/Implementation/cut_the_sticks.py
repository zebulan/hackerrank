from sys import stdin

next(stdin)
sticks = list(map(int, next(stdin).split()))
while sticks:
    smallest = min(sticks)
    tmp = []
    for i, stick in enumerate(sticks, 1):
        current = stick - smallest
        if current > 0:
            tmp.append(current)
    else:
        print(i)
    sticks = tmp
