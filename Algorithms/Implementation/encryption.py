from itertools import zip_longest
from math import ceil
from textwrap import wrap

txt = input()
print(' '.join(map(''.join, zip_longest(*wrap(txt, ceil(len(txt) ** 0.5)),
                                        fillvalue=''))))
