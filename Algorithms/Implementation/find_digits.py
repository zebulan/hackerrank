from sys import stdin

next(stdin)
for line in stdin:
    num = int(line)
    print(sum(not num % a for a in map(int, line.rstrip()) if a > 0))
