from string import ascii_lowercase as az, ascii_uppercase as AZ
from sys import stdin

next(stdin)
string = next(stdin)
shift = int(next(stdin))
result = []
for char in string:
    if char.islower():
        result.append(az[(az.index(char) + shift) % 26])
    elif char.isupper():
        result.append(AZ[(AZ.index(char) + shift) % 26])
    else:
        result.append(char)
print(''.join(result))
