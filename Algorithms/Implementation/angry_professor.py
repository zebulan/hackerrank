from sys import stdin

next(stdin)
for a, b in zip(stdin, stdin):
    _, k = map(int, a.split())
    print('YES' if sum(int(c) <= 0 for c in b.split()) < k else 'NO')
