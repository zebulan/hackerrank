from functools import reduce
from sys import stdin

next(stdin)
for line in stdin:
    print(reduce(lambda a, b: a + 1 if b % 2 else a * 2, range(int(line)), 1))
