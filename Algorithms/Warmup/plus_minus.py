from sys import stdin

next(stdin)
pos = neg = zero = total = 0
for num in map(int, next(stdin).split()):
    if not num:
        zero += 1
    elif num > 0:
        pos += 1
    else:
        neg += 1
    total += 1
print('{}\n{}\n{}'.format(pos / total, neg / total, zero / total))
