from operator import add as solve_me_first
from sys import stdin

print(solve_me_first(*map(int, stdin)))
