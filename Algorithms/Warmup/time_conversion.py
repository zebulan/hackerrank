from sys import stdin
from time import strptime, strftime

print(strftime('%H:%M:%S', strptime(next(stdin), '%I:%M:%S%p')))
