from sys import stdin

num = int(next(stdin))
[print('{:>{}}'.format('#' * a, num)) for a in range(1, num + 1)]
