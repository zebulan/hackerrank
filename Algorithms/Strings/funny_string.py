from string import ascii_lowercase as az
from sys import stdin

next(stdin)
for line in stdin:
    s = line.lower().rstrip()
    r = s[::-1]
    for a, b, c, d in zip(s[::1], s[1::1], r[::1], r[1::1]):
        if abs(az.index(a) - az.index(b)) != abs(az.index(c) - az.index(d)):
            print('Not Funny')
            break
    else:
        print('Funny')
