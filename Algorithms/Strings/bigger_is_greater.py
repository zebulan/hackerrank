from sys import stdin

next(stdin)
for line in stdin:
    current = list(line.rstrip())
    i = j = None
    less_val = ''
    for index, (a, b) in enumerate(zip(current, current[1::])):
        if a < b:
            i = index
            less_val = a
        if b > less_val:
            j = index + 1
    if i is None:
        print('no answer')
    else:
        current[i], current[j] = current[j], current[i]
        print(''.join(current[:i + 1] + current[i + 1:][::-1]))

# 1) Find the highest index i such that s[i] < s[i+1].
#    If no such index exists, the permutation is the last permutation.
# 2) Find the highest index j > i such that s[j] > s[i].
#    Such a j must exist, since i+1 is such an index.
# 3) Swap s[i] with s[j].
# 4) Reverse all the order of all of the elements after index i
