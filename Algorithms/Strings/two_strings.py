from sys import stdin

next(stdin)
for a, b in zip(stdin, stdin):
    print('YES' if set(a.rstrip()).intersection(b.rstrip()) else 'NO')
