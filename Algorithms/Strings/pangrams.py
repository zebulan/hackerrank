from string import ascii_lowercase
from sys import stdin

print(('not pangram', 'pangram')
      [set(ascii_lowercase).issubset(next(stdin).lower())])
