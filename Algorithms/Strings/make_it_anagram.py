from collections import Counter
from sys import stdin

a = Counter(next(stdin).rstrip())
b = Counter(next(stdin).rstrip())
print(sum(((a | b) - (a & b)).values()))
