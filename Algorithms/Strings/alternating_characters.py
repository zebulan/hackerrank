from itertools import groupby
from sys import stdin

next(stdin)
for line in stdin:
    print(sum(sum(1 for a in g) - 1 for _, g in groupby(line)))
