from collections import Counter
from sys import stdin


def pairs(nums, k):
    return sum(nums.get(key + k, 0) for key in nums.keys())

_, difference = map(int, next(stdin).split())
numbers = Counter(map(int, next(stdin).split()))
print(pairs(numbers, difference))
